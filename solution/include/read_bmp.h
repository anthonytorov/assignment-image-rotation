#ifndef READ_BMP_H
#define READ_BMP_H

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ALLOCATE_FAILED
};

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum read_status from_bmp( FILE* in_ptr, struct image** img_ptr );
enum write_status to_bmp( FILE* out_ptr, struct image const* img_ptr );

#endif
