#ifndef IMAGE_ROTATE_90DEG_CC_H
#define IMAGE_ROTATE_90DEG_CC_H

const struct maybe_allocated_image rotate_90deg_cc( struct image const source );

#endif
