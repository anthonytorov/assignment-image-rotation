#ifndef FILE_IO_H
#define FILE_IO_H

#include <stdbool.h>
#include <stdio.h>

enum file_mode  {
  READ = 0,
  WRITE,
  APPEND,
  OPEN_READ_AND_WRITE,
  CREATE_READ_AND_WRITE,
  OPEN_READ_AND_APPEND
};

bool open_file(const char* name, FILE** file_ptr_ptr, const enum file_mode mode );
bool close_file(FILE* file_ptr);

#endif
