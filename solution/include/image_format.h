#ifndef IMAGE_FORMAT_H
#define IMAGE_FORMAT_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct pixel { uint8_t b, g, r; };
struct pixel pixel_create(uint8_t r,uint8_t g,uint8_t b);

struct image {
  uint32_t width, height;
  struct pixel* data;
};

struct maybe_allocated_image {
  struct image* image_ptr;
  bool alloc_success;
};

struct maybe_allocated_image image_create(uint32_t width, uint32_t height);
bool image_set_dimensions_and_allocate(struct image* img_ptr, uint32_t width, uint32_t height);
void image_free_pixels(struct image* img_ptr);
size_t image_size_bytes(const struct image* img_ptr);
uint16_t image_bit_depth(void);
uint32_t image_compression_method(void);
uint32_t image_color_count(void);
uint32_t image_important_color_count(void);

bool image_get_pixel(const struct image* img_ptr, uint32_t row_num, uint32_t col_num, struct pixel* pxl_ptr);
void image_set_pixel(const struct image* img_ptr, uint32_t row_num, uint32_t col_num, const struct pixel pxl);

bool check_allocated(const struct maybe_allocated_image* maybe_image);
struct image* extract_image(const struct maybe_allocated_image* maybe_image);

#endif
