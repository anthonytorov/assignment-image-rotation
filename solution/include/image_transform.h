#ifndef IMAGE_TRANSFORM_H
#define IMAGE_TRANSFORM_H

#include <stdint.h>

#include "image_format.h"

typedef void linear_map(int64_t*, int64_t*);

typedef struct image_tranformation {
  linear_map* function_ptr;
} IMAGE_TRANSFORMATION;

struct maybe_allocated_image apply_linear_map( struct image const source, struct image_tranformation* transform );

#endif
