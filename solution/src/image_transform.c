#include "image_format.h"
#include "image_transform.h"

struct maybe_allocated_image apply_linear_map( struct image const source, IMAGE_TRANSFORMATION* transform ) {
    // transform image sizes:
    int64_t width_tf = (int64_t)source.width;
    int64_t height_tf = (int64_t)source.height;
    bool invert_cols = false, invert_rows = false;
    transform->function_ptr(&width_tf, &height_tf);

    if (width_tf < 0) { 
        invert_cols = true;
        width_tf *= -1;
    }
    if (height_tf < 0) {
        invert_rows = true;
        height_tf *= -1;
    }
    
    struct maybe_allocated_image transformed_image =  image_create(width_tf, height_tf);

    if (!check_allocated(&transformed_image)) return transformed_image; 
    
    struct pixel pxl = pixel_create(0,0,0);
    for (uint32_t row_num = 0; row_num < source.height; row_num++) {
        for (uint32_t col_num = 0; col_num < source.width; col_num++) {
            image_get_pixel(&source, row_num, col_num, &pxl);
            
            int64_t col_num_tf = (int64_t)col_num;
            int64_t row_num_tf = (int64_t)row_num;
            transform->function_ptr(&col_num_tf, &row_num_tf);
            if (invert_cols) { col_num_tf += width_tf - 1; }
            if (invert_rows) { row_num_tf += height_tf - 1; }

            image_set_pixel(transformed_image.image_ptr, row_num_tf, col_num_tf, pxl);
        }
    }

    return transformed_image;
}
