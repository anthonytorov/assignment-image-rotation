#include "file_io.h"

#include  <stdbool.h>
#include  <stdio.h>

bool open_file(const char* name, FILE** file_ptr_ptr, const enum file_mode mode ) {
    
    char* string_mode;
    switch (mode) {
        default:
            return false;
        case READ:
            string_mode = "r";
            break;
        case WRITE:
            string_mode = "w";
            break;
        case APPEND:
            string_mode = "a";
            break;
        case OPEN_READ_AND_WRITE:
            string_mode = "r+";
            break;
        case CREATE_READ_AND_WRITE:
            string_mode = "w+";
            break;
        case OPEN_READ_AND_APPEND:
            string_mode = "a+";
            break;
    }
    
    *file_ptr_ptr = fopen(name, string_mode);

    if (*file_ptr_ptr == NULL) {
        return false;
    }
    return true;
}

bool close_file(FILE* file_ptr) {
    fclose(file_ptr);
    return true;
}
