#include "image_format.h"
#include "image_transform.h"

static void transform_90deg_cc(int64_t* x_ptr, int64_t* y_ptr) { 
    int64_t neg_y = -*y_ptr;
    *y_ptr = *x_ptr;
    *x_ptr = neg_y;    
}

struct maybe_allocated_image rotate_90deg_cc( struct image const source ) { 
    IMAGE_TRANSFORMATION tf = {
        .function_ptr = &transform_90deg_cc
    };
    return apply_linear_map(source, &tf); 
}
