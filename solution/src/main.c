#include "file_io.h"
#include "image_format.h"
#include "image_rotate_90deg_cc.h"
#include "read_bmp.h"

#include <stdbool.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc < 3) {
        printf("Please specify source image path and transformed image path");
    }

    struct image* img_ptr = NULL;

    {
        FILE* in_ptr = NULL;
        bool open_successful = open_file(argv[1], &in_ptr, READ);

        if (!open_successful) { 
            fprintf(stderr, "failed to open file %s", argv[1]);
            return 1; 
        }

        char buf[2];
        fread(buf, sizeof(char), 2, in_ptr);
        if (buf[0] != 'B' || buf[1] != 'M') {
            fprintf(stderr, "encountered non-bmp file signature");
            return 1;
        }
        fseek(in_ptr, 0, SEEK_SET);

        enum read_status status_read = from_bmp(in_ptr, &img_ptr);
        if (status_read != READ_OK) {
            fprintf(stderr, "received NOT OK read status: %d", status_read);
        }

        close_file(in_ptr);
    }

    // woodoo transform magic
    struct maybe_allocated_image maybe_img_tfd = rotate_90deg_cc(*img_ptr);
    image_free_pixels(img_ptr);

    if (!maybe_img_tfd.alloc_success) {
        fprintf(stderr, "failed to allocate space for the transformed image");
        return 1;
    }

    {
        FILE* out_ptr = NULL;
        bool open_successful = open_file(argv[2], &out_ptr, WRITE);
        if (!open_successful) { 
            fprintf(stderr, "failed to open file %s", argv[1]);
            return 1; 
        }

        enum write_status status_write = to_bmp(out_ptr, maybe_img_tfd.image_ptr);
        if (status_write != WRITE_OK) {
            fprintf(stderr, "received WRITE_ERROR while writing %s", argv[1]);
        }
        close_file(out_ptr);
    }

    fprintf(stdout, "successfully written rotated image");
    image_free_pixels(maybe_img_tfd.image_ptr);

    return 0;
}
