#include "file_io.h"
#include "image_format.h"
#include "read_bmp.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
  uint16_t b_ftype;
  uint32_t b_file_size;
  uint32_t b_freserved;
  uint32_t b_off_bits;
  uint32_t bi_size;
  uint32_t bi_width;
  uint32_t bi_height;
  uint16_t bi_planes;
  uint16_t bi_bit_count;
  uint32_t bi_compression;
  uint32_t bi_size_image;
  uint32_t bi_xpels_per_meter;
  uint32_t bi_ypels_per_meter;
  uint32_t bi_clr_used;
  uint32_t bi_clr_important;
};
#pragma pack(pop)

/* mod = image_width % 4
* mod 0 -> pad 0
* mod 1 -> pad 3
* mod 2 -> pad 2
* mod 3 -> pad 1
*/

static uint32_t calculate_padding_bytes(const uint32_t image_width) {
  const uint32_t row_length_bytes = image_width * sizeof(struct pixel);
  if (row_length_bytes % 4 == 0) return 0;
  return (4 - row_length_bytes % 4);
}

static uint32_t calculate_raw_bitmap_size(struct image const* img_ptr) {
  const uint32_t padded_row_size_bytes = (img_ptr->width * sizeof(struct pixel)) + calculate_padding_bytes(img_ptr->width);
  return padded_row_size_bytes * img_ptr->height;
}

static uint32_t calculate_bmp_file_size(struct image const* img_ptr) {
  return sizeof(struct bmp_header) + calculate_raw_bitmap_size(img_ptr);
}

struct bmp_header bmp_header_create(struct image const* img_ptr) {
  return (struct bmp_header) {
    0x4D42, // BMP sign
    calculate_bmp_file_size(img_ptr),
    0, // reserved zeros
    sizeof(struct bmp_header), // offset is just the header
    40,
    img_ptr->width,
    img_ptr->height,
    1,
    image_bit_depth(), 
    image_compression_method(),
    calculate_raw_bitmap_size(img_ptr),
    2835,
    2835,
    image_color_count(),
    image_important_color_count()
  };
} 

static bool bmp_header_read(FILE* in_ptr, struct bmp_header* header_ptr) {
  size_t num_read = fread(header_ptr, sizeof(struct bmp_header), 1, in_ptr);
  if (num_read != 1) {
    return false;
  }
  return true;
}

static enum read_status bmp_header_check(const struct bmp_header header) {
  
  // check header field
  switch (header.b_ftype) {
    default:
      return READ_INVALID_SIGNATURE;
    case 0x4D42:
      break;
  }

  if (header.bi_bit_count > 24) {
    return READ_INVALID_BITS;
  }

  if (header.bi_planes != 1) {
    return READ_INVALID_HEADER;
  }

  return READ_OK;
}

enum read_status from_bmp( FILE* in_ptr, struct image** img_ptr_ptr ) {

  struct bmp_header header; 
  bool read_success = bmp_header_read(in_ptr, &header); 

  if (!read_success) { return READ_INVALID_HEADER; }
  enum read_status check_status = bmp_header_check(header);

  if (check_status != READ_OK) return check_status;

  if (*img_ptr_ptr != NULL) {
    image_free_pixels(*img_ptr_ptr);
  }

  struct maybe_allocated_image maybe_img = image_create(header.bi_width, header.bi_height);
  
  if (!maybe_img.alloc_success) {
    return READ_ALLOCATE_FAILED;
  }

  *img_ptr_ptr = maybe_img.image_ptr;

  fseek(in_ptr, header.b_off_bits, SEEK_SET);

  uint32_t padding = calculate_padding_bytes(header.bi_width);
  for (uint32_t row_num = 0; row_num < header.bi_height; row_num++) {
    // read data
    for (uint32_t col_num = 0; col_num < header.bi_width; col_num++) {
      struct pixel pixel_now = pixel_create(0,0,0);
      fread(&pixel_now, sizeof(struct pixel), 1, in_ptr); 
      image_set_pixel(*img_ptr_ptr, row_num, col_num, pixel_now);
    }
    // seek padding
    fseek(in_ptr, padding, SEEK_CUR);
  }
  
  return READ_OK;
}

enum write_status to_bmp( FILE* out_ptr, struct image const* img_ptr ) {
  
  struct bmp_header header = bmp_header_create(img_ptr);
  size_t num_written = fwrite(&header, sizeof(struct bmp_header), 1, out_ptr);
  if (num_written != 1) { return WRITE_ERROR; }

  const uint32_t padding = calculate_padding_bytes(header.bi_width);
  struct pixel pixel_to_write = pixel_create(0,0,0);
  
  for (uint32_t row_num = 0; row_num < header.bi_height; row_num++) {
    // read data
    for (uint32_t col_num = 0; col_num < header.bi_width; col_num++) {
      if (!image_get_pixel(img_ptr, row_num, col_num, &pixel_to_write)) {
        return WRITE_ERROR;
      }
      fwrite(&pixel_to_write, sizeof(struct pixel), 1, out_ptr); 
    }
    // seek padding
    const char padbyte = 0;
    for (uint32_t i = 0; i < padding; i++) { fwrite(&padbyte, sizeof(char), 1, out_ptr); }
  }
  
  return WRITE_OK;
}
