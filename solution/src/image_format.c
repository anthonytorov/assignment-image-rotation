#include "image_format.h"

#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>

struct pixel pixel_create(uint8_t r, uint8_t g, uint8_t b) { return (struct pixel) { b,g,r }; }

struct maybe_allocated_image image_create(uint32_t width, uint32_t height) {
  struct image created_image = (struct image) { 0, 0, NULL };
  bool allocate_success = image_set_dimensions_and_allocate(&created_image, width, height);

  return (struct maybe_allocated_image) { &created_image, allocate_success };
}

void image_free_pixels(struct image* img_ptr) {
  free(img_ptr->data);
}

static size_t image_pixel_length(const struct image* img_ptr) { return img_ptr->width * img_ptr->height; }
static size_t image_index_pixel_flat_rowfirst(const struct image* img_ptr, uint32_t row_num, uint32_t col_num) {
  return img_ptr->width * row_num + col_num;
}

uint16_t image_bit_depth(void) { return 24; }
uint32_t image_compression_method(void) { return 0; }
uint32_t image_color_count(void) { return 0; }
uint32_t image_important_color_count(void) { return 0; /* all colors are important */ }

bool image_set_dimensions_and_allocate(struct image* img_ptr, uint32_t width, uint32_t height) {
  
  if (img_ptr->data != NULL) {
    image_free_pixels(img_ptr);
  }
  
  img_ptr->width = width;
  img_ptr->height = height;
  img_ptr->data = malloc(sizeof(struct pixel) * image_pixel_length(img_ptr));

  return img_ptr->data != NULL;
}

bool image_get_pixel(const struct image* img_ptr, uint32_t row_num, uint32_t col_num, struct pixel* dest_ptr) {
  if (img_ptr->data == NULL) return false;

  size_t p_i = image_index_pixel_flat_rowfirst(img_ptr, row_num, col_num);
  // if out of array bounds
  if (p_i >= image_pixel_length(img_ptr)) return false;

  *dest_ptr = img_ptr->data[p_i];
  return true;
}

void image_set_pixel(const struct image* img_ptr, uint32_t row_num, uint32_t col_num, const struct pixel pxl) {
  if (img_ptr->data == NULL) return;

  size_t p_i = image_index_pixel_flat_rowfirst(img_ptr, row_num, col_num);
  // if out of array bounds
  if (p_i >= image_pixel_length(img_ptr)) return;

  img_ptr->data[p_i] = pxl;
}

bool check_allocated(const struct maybe_allocated_image* maybe_image) {
  return maybe_image->alloc_success;
}

struct image* extract_image(const struct maybe_allocated_image* maybe_image) {
  return maybe_image->image_ptr;
}
